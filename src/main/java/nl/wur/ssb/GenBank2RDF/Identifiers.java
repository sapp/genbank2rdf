package nl.wur.ssb.GenBank2RDF;

import java.util.regex.Pattern;

import nl.wur.ssb.RDFSimpleCon.data.Domain;
import nl.wur.ssb.RDFSimpleCon.data.Property;
import nl.wur.ssb.RDFSimpleCon.data.RDFSubject;
import nl.wur.ssb.RDFSimpleCon.data.RDFType;

public class Identifiers {

  public static String identifiersORG(String lookup, String lookupType) {
    lookup = lookup.replace("|", "");

    if (lookupType == "cdd") {
      return lookupType + ":" + lookup;
    }

    Pattern PROJECT = Pattern.compile("^PRJ[DEN][A-Z]\\d+$");
    if (PROJECT.matcher(lookup).find() && lookupType.contains("bioproject")) {
      return "http://identifiers.org/bioproject/" + lookup;
    }
    if (PROJECT.matcher(lookup).find() && lookupType.contains("project")) {
      return "http://identifiers.org/bioproject/" + lookup;
    }

    Pattern BIOSAMPLE = Pattern.compile("^SAM[NED](\\w)?\\d+$");
    if (BIOSAMPLE.matcher(lookup).find() && lookupType.contains("biosample")) {
      return "http://identifiers.org/biosample/" + lookup;
    }
    Pattern TAXONOMY = Pattern.compile("^\\d+$");
    if (TAXONOMY.matcher(lookup).find() && lookupType.contains("taxonomy|taxon")) {
      return "http://identifiers.org/taxonomy/" + lookup;
    }
    Pattern GI = Pattern.compile("^(GI|gi)\\:\\d+$");
    if (GI.matcher("gi:" + lookup).find() && lookupType.contains("gi")) {
      return "http://identifiers.org/ncbigi/" + lookup;
    }
    Pattern NCBIGENE = Pattern.compile("^(\\w+\\d+(\\.\\d+)?)|(NP_\\d+)$");
    if (NCBIGENE.matcher(lookup).find() && lookupType.contains("uniprotkb/swiss-prot")) {
      return "http://identifiers.org/ncbigene/" + lookup;
    }
    Pattern NCBIPUBMED = Pattern.compile("^\\d+$");
    if (NCBIPUBMED.matcher(lookup).find() && lookupType.contains("pubmed")) {
      return "http://identifiers.org/pubmed/" + lookup;
    }

    Pattern NCBIPROTEIN = Pattern.compile("^\\d+$");
    if (NCBIPROTEIN.matcher(lookup).find() && lookupType.contains("geneid")) {
      return "http://identifiers.org/ncbiprotein/" + lookup;
    }
    Pattern UNISTS = Pattern.compile("^\\d+$");
    if (UNISTS.matcher(lookup).find() && lookupType.contains("unists")) {
      return "http://identifiers.org/ncbigene/" + lookup;
    }

    Pattern REFSEQ = Pattern.compile(
        "^((AC|AP|NC|NG|NM|NP|NR|NT|NW|XM|XP|XR|YP|ZP)_\\d+|(NZ\\_[A-Z]{4}\\d+))(\\.\\d+)?$");
    if (REFSEQ.matcher(lookup).find() && lookupType.contains("refseq")) {
      return "http://identifiers.org/refseq/" + lookup;
    }
    // Included -.-.-.- as people tend to include that in genbank as
    // well....
    Pattern ENZYME = Pattern.compile(
        "^\\.-\\.-\\.-\\.-|\\d+\\.-\\.-\\.-|\\d+\\.\\d+\\.-\\.-|\\d+\\.\\d+\\.\\d+\\.-|\\d+\\.\\d+\\.\\d+\\.(n)?\\d+$");
    if (ENZYME.matcher(lookup).find() && lookupType.contains("enzyme")) {
      return "http://identifiers.org/ec-code/" + lookup;
    }
    Pattern INTERPRO = Pattern.compile("^IPR\\d{6}$");
    if (INTERPRO.matcher(lookup).find() && lookupType.contains("interpro")) {
      return "http://identifiers.org/interpro/" + lookup;
    }
    Pattern GO = Pattern.compile("^\\d{7}$");
    if (GO.matcher(lookup).find() && lookupType.contains("go")) {
      return "http://identifiers.org/go/" + lookup;
    }
    Pattern ENA = Pattern.compile("^[A-Z]+[0-9]+$");
    if (ENA.matcher(lookup).find() && lookupType.contains("ena")) {
      return "http://identifiers.org/ena.embl/" + lookup;
    }
    Pattern GOA = Pattern.compile(
        "^(([A-N,R-Z][0-9][A-Z][A-Z, 0-9][A-Z, 0-9][0-9])|([O,P,Q][0-9][A-Z, 0-9][A-Z, 0-9][A-Z, 0-9][0-9]))|(URS[0-9A-F]{10}(_[0-9]+){0,1})|(EBI-[0-9]+)$");
    if (GOA.matcher(lookup).find() && lookupType.contains("goa")) {
      return "http://identifiers.org/goa/" + lookup;
    }
    Pattern RFAM = Pattern.compile("^RF\\d{5}$");
    if (RFAM.matcher(lookup).find() && lookupType.contains("rfam")) {
      return "http://identifiers.org/rfam/" + lookup;
    }
    Pattern PDB = Pattern.compile("^[0-9][A-Za-z0-9]{3}$");
    if (PDB.matcher(lookup).find() && lookupType.contains("pdb")) {
      return "http://identifiers.org/pdb/" + lookup;
    }
    String lookupCABRI = lookup.replace(" ", "_");
    Pattern CABRI = Pattern.compile("^([A-Za-z]+)?([\\_])?([A-Za-z-]+)\\:([A-Za-z0-9 ]+)$");
    if (CABRI.matcher(lookupCABRI).find() && lookupType.contains("cabri")) {
      return "http://identifiers.org/cabri/" + lookup;
    }
    Pattern EUROPEPMC = Pattern.compile("^PMC\\d+$");
    if (EUROPEPMC.matcher(lookup).find() && lookupType.contains("europepmc")) {
      return "http://identifiers.org/pmc/" + lookup;
    }
    Pattern TEMPLATE = Pattern.compile("^TEMPLATE$");
    if (TEMPLATE.matcher(lookup).find() && lookupType.contains("template")) {
      return "http://identifiers.org/TEMPLATE/" + lookup;
    }
    Pattern DOI = Pattern.compile("^(doi\\:)?\\d{2}\\.\\d{4}.*$");
    if (DOI.matcher("doi:" + lookup).find() && lookupType.contains("doi")) {
      return "http://identifiers.org/doi/" + "doi:" + lookup;
    }
    // if
    // (lookupType.contains("refseq","uniprotkb/trembl","ena-con","md5","straininfo","cabri","silva-lsu","silva-ssu","ensemblgenomes-gn","ensemblgenomes-tr")){
    // return lookupType+":"+lookup
    // print ("RETURNING "+lookupType+":"+lookup)

    return lookupType + ":" + lookup;

  }

  public static void biopax(String value, RDFSubject featureURI, Domain domain) {
    new RDFType(domain, "biopax:Xref", false);
    new Property(domain, "biopax:db", false);
    new Property(domain, "biopax:id", false);
    new Property(domain, "ssb:xref", false);
    value = value.replace("|", "_");
    String lookupType = value.split(":")[0].trim().toLowerCase();
    String lookup = value.split(":")[1].trim();
    String url = identifiersORG(lookup, lookupType);
    RDFSubject lookupURI;
    if (url.startsWith("http")) {
      lookupURI = new RDFSubject(domain, url, "biopax:Xref");

    } else {

      lookupURI = new RDFSubject(domain, "ssb:" + value.replace(" ", "_"), "biopax:Xref");
    }
    featureURI.add("ssb:xref", lookupURI);
    lookupURI.addLit("biopax:db", lookupType);
    lookupURI.addLit("biopax:id", lookup);

  }

}
