package nl.wur.ssb.GenBank2RDF;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Checksum {

  public static String generator(String forCheckSum) {

    try {
      MessageDigest md = MessageDigest.getInstance("SHA-384");
      md.update(forCheckSum.trim().getBytes());
      byte[] digest = md.digest();
      StringBuffer sb = new StringBuffer();
      for (byte b : digest) {
        sb.append(String.format("%02x", b & 0xff));
      }

      return (sb.toString());
    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    }
    return null;
  }

  public static String generator2(String forChecksum) throws NoSuchAlgorithmException {

    MessageDigest md = MessageDigest.getInstance("SHA-384");
    md.update(forChecksum.getBytes());

    byte byteData[] = md.digest();

    // convert the byte to hex format method 1
    StringBuffer sb = new StringBuffer();
    for (int i = 0; i < byteData.length; i++) {
      sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
    }
    return sb.toString();

  }
}
