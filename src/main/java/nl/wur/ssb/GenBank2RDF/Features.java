package nl.wur.ssb.GenBank2RDF;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import org.biojava.nbio.core.exceptions.CompoundNotFoundException;
import org.biojava.nbio.core.sequence.DNASequence;
import org.biojava.nbio.core.sequence.template.Sequence;
import org.biojava.nbio.core.sequence.template.SequenceView;
import org.biojava.nbio.core.sequence.transcription.Frame;
import org.biojava.nbio.core.sequence.transcription.TranscriptionEngine;
import org.codehaus.plexus.util.StringUtils;

import nl.wur.ssb.GenBankHandler.data.Feature;
import nl.wur.ssb.GenBankHandler.data.Qualifier;
import nl.wur.ssb.GenBankHandler.data.QualifierValue;
import nl.wur.ssb.RDFSimpleCon.data.Domain;
import nl.wur.ssb.RDFSimpleCon.data.Property;
import nl.wur.ssb.RDFSimpleCon.data.RDFSubject;
import nl.wur.ssb.RDFSimpleCon.data.RDFType;

public class Features {

  public static void Creator(ArrayList<Feature> features, RDFSubject dnaobjectRDF, Domain domain,
      Sequence<?> dnasequence, String Source, int codon)
      throws CompoundNotFoundException, NoSuchAlgorithmException {
    int startPos = 0;
    int endPos = 0;
    RDFSubject featureURI = null;

    for (Feature feature : features) {
      String key = feature.getKey();
      if (key.equals("source")) {
        for (Qualifier qualifier : feature.getAllQualifiers()) {
          if (qualifier.getKey().equals("db_xref")) {
            for (QualifierValue value : qualifier.getValues()) {
              Identifiers.biopax(value.getVal().replaceAll("^\"|\"$", ""), dnaobjectRDF, domain);
            }
          } else {
            for (QualifierValue value : qualifier.getValues()) {
              new Property(domain, "ssb:" + qualifier.getKey(), false);
              try {
                dnaobjectRDF.addLit("ssb:" + qualifier.getKey(),
                    value.getVal().replaceAll("^\"|\"$", ""));
              } catch (Exception e) {
                // For cases when a variable is given and assumes it is true
                dnaobjectRDF.addLit("ssb:" + qualifier.getKey(), "true");
              }
            }
          }
        }
        // } else if (key.equals("gene")) {
        // // TODO Think of a fix for genbanks having no gene identifers,
        // // now it is solved like this
        // startPos = feature.getLocation().getBeginPosition();
        // endPos = feature.getLocation().getEndPosition();
        // if (startPos < endPos) {
        // startPos++;
        // } else {
        // endPos++;
        // }
        //
        // new RDFType(domain, "ssb:" +
        // StringUtils.capitalise(key.toLowerCase()), false);
        //
        // featureURI = new RDFSubject(domain,
        // dnaobjectRDF.toString().replace("<", "").replace(">", "") + "/"
        // + feature.getLocation().getBeginPosition() + "_" +
        // feature.getLocation().getEndPosition(),
        // "ssb:" + StringUtils.capitalise(key.toLowerCase()));
        // featureURI.addLit("ssb:source", Source);
        // featureURI.add("ssb:begin", startPos);
        // featureURI.add("ssb:end", endPos);

      } else {
        int subStartPos = feature.getLocation().getBeginPosition();
        int subEndPos = feature.getLocation().getEndPosition();
        if (subStartPos < subEndPos) {
          subStartPos++;
        } else {
          subEndPos++;
        }

        // TODO check if begin / end fall within GENEpos, if so then it
        // is a subfeature off....
        if (startPos == subStartPos && endPos == subEndPos) {

          new RDFType(domain, "ssb:" + StringUtils.capitalise(key.toLowerCase()), false);

          featureURI = new RDFSubject(domain,
              dnaobjectRDF.toString().replace("<", "").replace(">", "") + "/"
                  + feature.getLocation().getBeginPosition() + "_"
                  + feature.getLocation().getEndPosition(),
              "ssb:" + StringUtils.capitalise(key.toLowerCase()));
          dnaobjectRDF.add("ssb:feature", featureURI);
          featureURI.addLit("ssb:source", Source);
        } else if (startPos <= subStartPos && endPos >= subEndPos) {
          new RDFType(domain, "ssb:" + StringUtils.capitalise(key.toLowerCase()), false);
          RDFSubject subFeatureURI = new RDFSubject(domain,
              dnaobjectRDF.toString().replace("<", "").replace(">", "") + "/"
                  + feature.getLocation().getBeginPosition() + "_"
                  + feature.getLocation().getEndPosition(),
              "ssb:" + StringUtils.capitalise(key.toLowerCase()));

          featureURI.add("ssb:feature", subFeatureURI);
          featureURI.addLit("ssb:source", Source);
          featureURI = subFeatureURI;
        } else {
          // NOT FITTING THUS NEW THING

          new RDFType(domain, "ssb:" + StringUtils.capitalise(key.toLowerCase()), false);
          featureURI = new RDFSubject(domain,
              dnaobjectRDF.toString().replace("<", "").replace(">", "") + "/"
                  + feature.getLocation().getBeginPosition() + "_"
                  + feature.getLocation().getEndPosition(),
              "ssb:" + StringUtils.capitalise(key.toLowerCase()));
          dnaobjectRDF.add("ssb:feature", featureURI);
          featureURI.addLit("ssb:source", Source);
        }

        boolean hasProteinSequence = false;
        for (Qualifier qualifier : feature.getAllQualifiers()) {
          String qkey = qualifier.getKey();
          List<QualifierValue> qvalues = qualifier.getValues();
          for (QualifierValue qvalue : qvalues) {
            if (qkey != null && qvalue == null) {
              // Properties with no value is added such as /pseudo
              // or /ribosomal_slippage
              new Property(domain, "ssb:" + qkey.toLowerCase(), false);
              featureURI.addLit("ssb:" + qkey.toLowerCase(), "True");
            } else if (qvalue == null || qkey == null) {

            } else if (qkey.equals("translation")) {
              hasProteinSequence = true;
              String sequence = qvalue.getVal().replaceAll(" ", "").replaceAll("^\"|\"$", "");
              String checksum = Checksum.generator(sequence);
              RDFSubject proteinURI =
                  new RDFSubject(domain, "ssb:protein/" + checksum, "ssb:Protein");
              featureURI.add("ssb:protein", proteinURI);
              proteinURI.addLit("ssb:sequence", sequence);
              proteinURI.addLit("ssb:sha384", checksum);
            } else if (qkey.equals("db_xref")) {
              Identifiers.biopax(qvalue.getVal().replaceAll("^\"|\"$", ""), featureURI, domain);
            } else {
              new Property(domain, "ssb:" + qkey.toLowerCase(), false);
              if (qkey.equals("transl_table") || qkey.equals("codon_start")) {
                featureURI.add("ssb:" + qkey.toLowerCase(),
                    Integer.parseInt(qvalue.getVal().replaceAll("^\"|\"$", "")));
              } else {
                featureURI.addLit("ssb:" + qkey.toLowerCase(),
                    qvalue.getVal().replaceAll("^\"|\"$", ""));
              }

            }
          }
        }

        featureURI.add("ssb:begin", subStartPos);
        featureURI.add("ssb:end", subEndPos);
        String subSequence = null;

        if (dnasequence != null) {
          if (subStartPos < subEndPos) {
            subSequence = dnasequence.getSubSequence(subStartPos, subEndPos).getSequenceAsString();
            String checksum = Checksum.generator(subSequence.replaceAll(" ", ""));
            featureURI.addLit("ssb:sequence", subSequence);
            featureURI.addLit("ssb:sha384", checksum);
          } else {
            SequenceView<?> reverseSequence = dnasequence.getSubSequence(subEndPos, subStartPos);
            subSequence = new DNASequence(reverseSequence.getSequenceAsString())
                .getReverseComplement().getSequenceAsString();
            String checksum = Checksum.generator(subSequence.replaceAll(" ", ""));
            featureURI.addLit("ssb:sequence", subSequence);
            featureURI.addLit("ssb:sha384", checksum);
          }
        }

        if (!hasProteinSequence) {
          if (key.equals("CDS")) {
            if (subSequence != null && subSequence.length() % 3 == 0) {
              TranscriptionEngine.Builder b = new TranscriptionEngine.Builder();
              b.table(codon).initMet(true).trimStop(true);
              TranscriptionEngine engine = b.build();
              String protein = new DNASequence(subSequence).getRNASequence(Frame.ONE)
                  .getProteinSequence(engine).getSequenceAsString();
              // Pseudo genes containing stop codons and joins are
              // not processed at the moment
              if (!protein.contains("*")) {
                String checksum = Checksum.generator(protein);
                RDFSubject proteinURI =
                    new RDFSubject(domain, "ssb:protein/" + checksum, "ssb:Protein");
                featureURI.add("ssb:protein", proteinURI);
                proteinURI.addLit("ssb:sequence", protein);
                proteinURI.addLit("ssb:sha384", checksum);
              }
            }
          }
        }

        for (Qualifier qualifier : feature.getAllQualifiers()) {
          key = qualifier.getKey();
          List<QualifierValue> values = qualifier.getValues();
          for (QualifierValue value : values) {
            if (value == null) {
            } else if (key.equals("translation")) {
              String sequence = value.getVal().replaceAll(" ", "").replaceAll("^\"|\"$", "");
              String checksum = Checksum.generator(sequence);
              RDFSubject proteinURI =
                  new RDFSubject(domain, "ssb:protein/" + checksum, "ssb:Protein");
              featureURI.add("ssb:protein", proteinURI);
              proteinURI.addLit("ssb:sequence",
                  value.getVal().replaceAll(" ", "").replaceAll("^\"|\"$", ""));
            } else if (key.equals("db_xref")) {
              Identifiers.biopax(value.getVal().replaceAll("^\"|\"$", ""), featureURI, domain);
            } else {
              new Property(domain, "ssb:" + key.toLowerCase(), false);
              if (key.equals("transl_table") || key.equals("codon_start")) {
                featureURI.add("ssb:" + key.toLowerCase(),
                    Integer.parseInt(value.getVal().replaceAll("^\"|\"$", "")));
              } else {
                featureURI.addLit("ssb:" + key.toLowerCase(),
                    value.getVal().replaceAll("^\"|\"$", ""));
              }
            }
          }
        }
      }
    }
  }
}
