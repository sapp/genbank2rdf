package nl.wur.ssb.GenBank2RDF;

import java.util.Calendar;

import nl.wur.ssb.GenBankHandler.data.CrossRef;
import nl.wur.ssb.GenBankHandler.data.DateVersion;
import nl.wur.ssb.GenBankHandler.data.Record;
import nl.wur.ssb.GenBankHandler.data.Reference;
import nl.wur.ssb.GenBankHandler.data.location.Accession;
import nl.wur.ssb.RDFSimpleCon.data.Domain;
import nl.wur.ssb.RDFSimpleCon.data.Property;
import nl.wur.ssb.RDFSimpleCon.data.RDFSubject;

public class DnaObject {

  static void Creator(Record record, RDFSubject dnaobjectRDF, Domain domain, RDFSubject genomeRDF,
      String Source) {
    genomeRDF.add("ssb:dnaobject", dnaobjectRDF);
    dnaobjectRDF.addLit("ssb:source", Source);
    if (record.accessions != null) {
      for (Accession accession : record.accessions)
        dnaobjectRDF.addLit("ssb:accessions", accession.toString());
    }
    // if (record.baseCount != null)
    // dnaobjectRDF.add("ssb:baseCount", record.baseCount);
    dnaobjectRDF.add("ssb:circular", record.isCircular());
    if (record.comment != null)
      dnaobjectRDF.addLit("ssb:comment", record.comment);
    if (record.contigLocation != null)
      dnaobjectRDF.addLit("ssb:contigLocation", record.contigLocation);
    if (record.data_file_division != null)
      dnaobjectRDF.addLit("ssb:data_file_division", record.data_file_division);
    if (record.dates != null) {

      long timeStamp;
      for (DateVersion datum : record.dates) {
        timeStamp = datum.getDate().getTime();
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timeStamp);
        int mYear = calendar.get(Calendar.YEAR);
        int mMonth = calendar.get(Calendar.MONTH);
        int mDay = calendar.get(Calendar.DAY_OF_MONTH);

        dnaobjectRDF.addLit("ssb:dates", mYear + "-" + mMonth + "-" + mDay);
      }
    }
    if (record.db_source != null)
      dnaobjectRDF.addLit("ssb:db_source", record.db_source);
    if (record.dblinks != null) {
      for (CrossRef dblink : record.dblinks) {
        Identifiers.biopax(dblink.toString().replaceAll("^\"|\"$", ""), dnaobjectRDF, domain);
      }
    }

    if (record.definition != null)
      dnaobjectRDF.addLit("ssb:definition", record.definition);
    if (record.gi != null)
      dnaobjectRDF.addLit("ssb:gi", record.gi);
    if (record.keywords != null) {
      for (String keyword : record.keywords) {
        dnaobjectRDF.addLit("ssb:keywords", keyword);
      }
    }
    if (record.getLocus() != null)
      dnaobjectRDF.addLit("ssb:locus", record.getLocus());
    if (record.nid != null)
      dnaobjectRDF.addLit("ssb:nid", record.nid);
    if (record.organelle != null)
      dnaobjectRDF.addLit("ssb:organelle", record.organelle);
    if (record.organism != null)
      dnaobjectRDF.addLit("ssb:organism", record.organism);
    if (record.originName != null)
      dnaobjectRDF.addLit("ssb:originName", record.originName);
    if (record.pid != null)
      dnaobjectRDF.addLit("ssb:pid", record.pid);
    if (record.references != null) {
      // TODO fix referencing
      for (Reference reference : record.references) {
        if (reference != null) {
          new Property(domain, "ssb:consortium", false);
          new Property(domain, "ssb:journal", false);
          new Property(domain, "ssb:remark", false);
          new Property(domain, "ssb:title", false);
          new Property(domain, "ssb:authors", false);

          RDFSubject articleRDF = new RDFSubject(domain,
              genomeRDF.toString().replace("<", "").replace(">", "") + "/article_" + reference.id,
              "ssb:Article");
          genomeRDF.add("ssb:article", articleRDF);
          String authors = reference.authors;
          if (!(authors == null)) {
            for (String author : authors.split(",")) {
              if (!author.isEmpty())
                articleRDF.addLit("ssb:authors", author);
            }
          }
          if (reference.consrtm != null)
            articleRDF.addLit("ssb:consortium", reference.consrtm);
          if (reference.journal != null)
            articleRDF.addLit("ssb:journal", reference.journal);
          if (reference.remark != null)
            articleRDF.addLit("ssb:remark", reference.remark);
          if (reference.title != null)
            articleRDF.addLit("ssb:title", reference.title);
          for (CrossRef cross : reference.crossRefs) {
            String id = cross.getId();
            String db = cross.getDb();
            Identifiers.biopax(db + ":" + id, genomeRDF, domain);
          }
          // articleRDF.addLit("ssb:title", );
        }
      }
    }
    // if (record.residueType != null)
    // TODO check residueType
    // dnaobjectRDF.addLit("ssb:residueType", record.residueType);
    if (record.segment != null)
      dnaobjectRDF.addLit("ssb:segment", record.segment);
    if (record.getSequence() != null) {
      dnaobjectRDF.addLit("ssb:sequence", record.getSequence());
      dnaobjectRDF.addLit("ssb:sha384", Checksum.generator(record.getSequence()));

    }
    if (record.getSize() != -1)
      dnaobjectRDF.add("ssb:size", record.getSize());
    if (record.source != null)
      dnaobjectRDF.addLit("ssb:source", record.source);
    if (record.getStrandType() != null)
      dnaobjectRDF.addLit("ssb:strandType", record.getStrandType().toString());
    if (record.taxDivision != null)
      dnaobjectRDF.addLit("ssb:taxDivision", record.taxDivision);

    if (record.taxonomy != null)
      for (String taxon : record.taxonomy) {
        dnaobjectRDF.addLit("ssb:taxonomy", taxon);
      }
    if (record.wgs != null)
      dnaobjectRDF.addLit("ssb:wgs", record.wgs);
    if (record.wgsScaffold != null)
      dnaobjectRDF.addLit("ssb:wgsScaffold", record.wgsScaffold);

  }
}
