package nl.wur.ssb.GenBank2RDF;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;

public class CommandParser {

  public static CommandLine argsValidator(String[] args) throws Exception {
    Options options = new Options();

    // input
    Option option = Option.builder("i").argName("file").desc("Fasta file input").hasArg(true)
        .required(true).longOpt("input").build();
    options.addOption(option);
    // output
    option = Option.builder("o").argName("file").hasArg(true).desc("Output filename").required(true)
        .longOpt("output").build();
    options.addOption(option);
    // Data Format used
    option = Option.builder("f").longOpt("format").argName("gbk/embl").hasArg(true)
        .desc("EMBL/Genbank format used").required(true).build();
    options.addOption(option);
    // Data Format used
    option = Option.builder("c").longOpt("codon").argName("4/11").hasArg(true)
        .desc("Codon table used").required(true).build();
    options.addOption(option);
    // Genome Identifier, if wanted
    option = Option.builder("id").longOpt("identifier").argName("identifier").hasArg(true)
        .desc("Genome identification tag if required").required(false).build();
    options.addOption(option);
    // Source
    option = Option.builder("s").argName("NCBI/RAST/SAPP").hasArg(true)
        .desc("Source of genbank (NCBI/RAST/SAPP)").required(true).longOpt("source").build();
    options.addOption(option);
    //
    CommandLineParser parser = new DefaultParser();
    try {
      CommandLine cmd = parser.parse(options, args);
      return cmd;
    } catch (Exception e) {
      System.out.println(e);
      HelpFormatter formatter = new HelpFormatter();
      formatter.printHelp("Missing arguments, possible options see below", options);

    }
    System.exit(0);
    return null;

  }

}
