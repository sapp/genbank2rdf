package nl.wur.ssb.GenBank2RDF;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.zip.GZIPInputStream;

import org.apache.commons.cli.CommandLine;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.biojava.nbio.core.sequence.DNASequence;
import org.rdfhdt.hdt.exceptions.ParserException;

import nl.wur.ssb.GenBankHandler.data.Record;
import nl.wur.ssb.GenBankHandler.data.RecordBuilder;
import nl.wur.ssb.GenBankHandler.parser.EmblParser;
import nl.wur.ssb.GenBankHandler.parser.GenBankParser;
import nl.wur.ssb.GenBankHandler.parser.InsdcParser;
import nl.wur.ssb.HDT.HDT;
import nl.wur.ssb.RDFSimpleCon.RDFSimpleCon;
import nl.wur.ssb.RDFSimpleCon.data.Domain;
import nl.wur.ssb.RDFSimpleCon.data.Property;
import nl.wur.ssb.RDFSimpleCon.data.RDFSubject;
import nl.wur.ssb.RDFSimpleCon.data.RDFType;

public class App {

  private static String OrganismID = "Unknown";
  private static RDFSimpleCon SAPPSource;
  private static Domain domain;
  private static CommandLine arguments;
  private static final Logger logger = Logger.getLogger(App.class);

  public static void main(String[] args) throws Exception {
    arguments = CommandParser.argsValidator(args);
    // Initialise logger
    BasicConfigurator.configure(); // enough for configuring log4j
    Logger.getRootLogger().setLevel(Level.INFO); // changing log level

    InitialiseRDF();
    InitialiseClasses();
    InitialisePredicates();
    Initialise();
    save();
  }

  private static void save() throws IOException, ParserException {
    HDT hdt = new HDT();
    hdt.save(SAPPSource, arguments.getOptionValue("output"));
  }

  private static void InitialisePredicates() {
    new Property(domain, "ssb:sha384");
    new Property(domain, "ssb:accessions");
    new Property(domain, "ssb:baseCount");
    new Property(domain, "ssb:circular");
    new Property(domain, "ssb:comment");
    new Property(domain, "ssb:contigLocation");
    new Property(domain, "ssb:data_file_division");
    new Property(domain, "ssb:dates");
    new Property(domain, "ssb:db_source");
    new Property(domain, "ssb:dblinks");
    new Property(domain, "ssb:definition");
    new Property(domain, "ssb:gi");
    new Property(domain, "ssb:keywords");
    new Property(domain, "ssb:locus");
    new Property(domain, "ssb:nid");
    new Property(domain, "ssb:organelle");
    new Property(domain, "ssb:organism");
    new Property(domain, "ssb:originName");
    new Property(domain, "ssb:pid");
    new Property(domain, "ssb:references");
    new Property(domain, "ssb:residueType");
    new Property(domain, "ssb:segment");
    new Property(domain, "ssb:sequence");
    new Property(domain, "ssb:size");
    new Property(domain, "ssb:source");
    new Property(domain, "ssb:strandType");
    new Property(domain, "ssb:taxDivision");
    new Property(domain, "ssb:taxonomy");
    new Property(domain, "ssb:wgs");
    new Property(domain, "ssb:wgsScaffold");
    new Property(domain, "ssb:feature");
    new Property(domain, "ssb:begin");
    new Property(domain, "ssb:end");
    new Property(domain, "ssb:protein");
    new Property(domain, "ssb:article");
    new Property(domain, "ssb:dnaobject");

  }

  private static void InitialiseClasses() {
    new RDFType(domain, "ssb:Genome");
    new RDFType(domain, "ssb:DnaObject");
    new RDFType(domain, "ssb:Cds");
    new RDFType(domain, "ssb:Protein");
    new RDFType(domain, "ssb:Article");

  }

  private static void InitialiseRDF() throws Exception {
    SAPPSource = new RDFSimpleCon("");
    SAPPSource.setNsPrefix("ssb", "http://csb.wur.nl/genome/");
    SAPPSource.setNsPrefix("biopax", "http://www.biopax.org/release/bp-level3.owl#");
    domain = new Domain(SAPPSource);
  }

  private static void Initialise() throws Exception {
    RecordBuilder builder = new RecordBuilder();

    InsdcParser parser = null;
    String Format = arguments.getOptionValue("format");
    String Source = arguments.getOptionValue("source");
    int codon = Integer.parseInt(arguments.getOptionValue("codon"));

    if (arguments.getOptionValue("input").endsWith("gz")) {
      GZIPInputStream in =
          new GZIPInputStream(new FileInputStream(arguments.getOptionValue("input")));
      if (Format.toLowerCase().equals("embl")) {
        System.out.println("Parsing as EMBL GZip");
        parser = new EmblParser(in, builder);
      } else if (Format.toLowerCase().equals("gbk")) {
        System.out.println("Parsing as Genbank GZip");
        parser = new GenBankParser(in, builder);
      } else {
        in.close();
        throw new IllegalArgumentException("only embl or gbk are allowed, use -format embl or gbk");
      }
    } else {
      FileInputStream in = new FileInputStream(arguments.getOptionValue("input"));
      if (Format.toLowerCase().equals("embl")) {
        System.out.println("Parsing as EMBL");
        parser = new EmblParser(in, builder);
      } else if (Format.toLowerCase().equals("gbk")) {
        System.out.println("Parsing as Genbank");
        parser = new GenBankParser(in, builder);
      } else {
        in.close();
        throw new IllegalArgumentException("only embl or gbk are allowed, use -format embl or gbk");
      }
    }

    String recordID = null;
    int recordSize = 0;
    while (recordID == null) {
      parser.parse(true);
      if (builder.getRecords().size() != recordSize) {
        parser.parse(true);
        recordSize = builder.getRecords().size();
      } else {
        for (Record record : builder.getRecords()) {
          if (arguments.hasOption("id")) {
            // If ID tag is given use that as organism ID
            OrganismID = stringCleaner(arguments.getOptionValue("id"));
          } else if (record.organism != null) {

            OrganismID = stringCleaner(record.organism);
          }

          RDFSubject genomeRDF = new RDFSubject(domain, "ssb:" + OrganismID, "ssb:Genome");

          if (record.organism != null) {
            genomeRDF.addLit("ssb:organism", record.organism);
          }

          genomeRDF.addLit("ssb:source", Source);
          String sequence = record.getSequence();

          // temporary solution for sequences that are to bigg
          String checksum = Checksum.generator(sequence != null ? sequence : "no-sequence");

          DNASequence dnasequence = null;
          if (sequence != null)
            dnasequence = new DNASequence(sequence.replaceAll("[^atgc]", "n"));

          RDFSubject dnaobjectRDF =
              new RDFSubject(domain, "ssb:" + OrganismID + "/" + checksum, "ssb:DnaObject");
          DnaObject.Creator(record, dnaobjectRDF, domain, genomeRDF, Source);

          Features.Creator(record.features, dnaobjectRDF, domain, dnasequence, Source, codon);
        }
        recordID = "done";
      }
    }
  }

  private static String stringCleaner(String string) {
    return string.replace(" ", "_").replace("'", "").replace("\"", "").replace("<i>", "")
        .replace("</i>", "").replace("<", "").replace(">", "").replace(" ", "_").replace("/", "")
        .replace("(", "").replace(")", "").replace("[", "").replace("]", "");

  }

}
