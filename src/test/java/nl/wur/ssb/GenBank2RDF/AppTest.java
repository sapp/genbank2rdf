package nl.wur.ssb.GenBank2RDF;

import junit.framework.TestCase;
import nl.wur.ssb.RDFSimpleCon.Util;

/**
 * Unit test for simple App.
 */
public class AppTest extends TestCase {

  public void testEMBL() throws Exception {
    String[] args2 = {"-input", "./src/test/resources/AAGK01000009.gz", "-format", "embl",
        "-output", "./src/test/resources/galaxy.hdt", "-source", "ENA", "-codon", "11"};
    App.main(args2);
  }
}
